/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.actia.control;

import com.actia.coreican.services.BrandServices;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import org.json.JSONException;

/**
 *
 * @author yyadine
 */
@Path("brands")
public class Brands {
    
    @Path("getMethod/{name}")
    @GET
    public Response getBrands(@PathParam("name") String name)
    {
        com.actia.coreican.services.BrandServices bs = new BrandServices();
        String result = null;
        try {
            result = bs.getBrands();
           // System.out.println(result);
        } catch (JSONException ex) {
            Logger.getLogger(Brands.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Brands.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(result).build();
    }
}
