/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.actia.control;

import com.actia.coreican.services.BrandServices;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;

/**
 *
 * @author yyadine
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        com.actia.coreican.services.BrandServices bs = new BrandServices();
        try {
            String result = bs.getBrands();
            System.out.println(result);
        } catch (JSONException ex) {
            Logger.getLogger(Brands.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Brands.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
